package com.ex.ood;

import java.awt.*;
import java.awt.event.*;
import com.ex.ood.*;

public class Graphic extends Frame{
    Oval ov1 = new Oval(30,20,60,60);
    Oval ov2 = new Oval(40,30,50,60);
    FilledOval fOv = new FilledOval(10,20,30,60);
    
    public void paint(Graphics g){
    ov1.draw(g);
    ov2.draw(g);
    fOv.draw(g);
    }

    public Graphic(){
    addWindowListener(new WindowAdapter(){
    public void windowClosing(WindowEvent e){
    dispose();
    System.exit(0);
    }});
    }
}
