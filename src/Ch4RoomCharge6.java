package com.ex.ood;

public class Ch4RoomCharge6
{
    //Korea Hotel
    private int room;
    private int meal;
    private int movie;
    private int sum;

    public Ch4RoomCharge6(int room, int meal, int movie){
        this.room = room;
        this.meal = meal;
        this.movie = movie;
    }
    public int getTotalBill()
    {
        final int roomCharge = room;
        int mealCharge =meal;
        int movieCharge = movie;
        
        return roomCharge + mealCharge + movieCharge;
   }
}
