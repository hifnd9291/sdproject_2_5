package com.ex.ood;

import com.ex.ood.*;
import java.util.Date;

public class Ch3Student
{
    private Ch2Person me;
    private float grade;

    public Ch3Student(Ch2Person me, float grade)
    {
        this.me=me;
        this.grade = grade;
    }

    public float getGrade(){
        return this.grade;
    }
    public String getName(){
        return me.getName();}

    public Date getBirthdate(){
        return me.getBirthdate();}
}
