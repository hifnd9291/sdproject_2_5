package com.ex.ood;

import java.awt.Point;
import java.awt.Rectangle;

public class Ch2EnhancedRectangle extends Rectangle{
    public Ch2EnhancedRectangle(int x,int y, int w, int h) {
        super(x, y, w, h);
    }
    public Point getCenter() {
        return new Point((int) getCenterX(), (int) getCenterY());
    }
    public void setCenter(int x, int y) {
        setLocation(x - (int) getWidth()/2, y- (int)getHeight()/2);
    }
}
