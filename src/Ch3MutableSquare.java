package com.ex.ood;

import com.ex.ood.*;

class Ch3MutableSquare extends Ch3Rectangle{

    public Ch3MutableSquare(int x,int y, int width){
        super(x,y,width,width);
    }
    public void setSize(int w){
        this.width =w;
        this.height=w;
    }
}
