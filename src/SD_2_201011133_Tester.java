package com.ex.ood;

import com.ex.ood.*;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.awt.Color;

/**
@author naivewhim
@version 1.0
@since 2013.10.07
*/

public class SD_2_201011133_Tester{
    public static void main(String[] args) {
       ch4main();
       // ch3main();
       // ch2main();
    }
    private static void ch4main(){
    System.out.println("--Ch4- Refactoring");
    //Implement getRoomcharge
    Ch4RoomCharge1 rc1 = new Ch4RoomCharge1(10,20,30);
    System.out.println("getRoomCharge(): " + rc1.getRoomCharge());
    //renaming step 1.
    Ch4RoomCharge2 rc2 = new Ch4RoomCharge2(10,20,30);
    System.out.println("Rename method step1");
     System.out.println("getTotalBill(): " + rc2.getTotalBill());
    System.out.println("getRoomCharge(): " + rc2.getRoomCharge());    
    //renaming step 2.
    Ch4RoomCharge3 rc3 = new Ch4RoomCharge3(10,20,30);
    System.out.println("Rename method step2"); 
    System.out.println("getTotalBill(): " + rc3.getTotalBill());
    System.out.println("getRoomCharge(): " + rc3.getRoomCharge());
    //renaming step 3.
    Ch4RoomCharge4 rc4 = new Ch4RoomCharge4(10,20,30);
    System.out.println("Rename method step3"); 
    System.out.println("getTotalBill(): " + rc4.getTotalBill());
    System.out.println("getRoomCharge(): " + rc4.getRoomCharge());
    //renaming step 4.
    Ch4RoomCharge5 rc5 = new Ch4RoomCharge5(10,20,30);
    System.out.println("Rename method step4"); 
    System.out.println("getTotalBill(): " + rc5.getTotalBill());
 
    //Introduce Explaining Variable
    Ch4RoomCharge6 rc6 = new Ch4RoomCharge6(10,20,30);
    System.out.println("Introduce Explaining Variable");
    System.out.println("getTotalBill(): " + rc6.getTotalBill());

    //Replace Temp with Query
    Ch4RoomCharge7 rc7 = new Ch4RoomCharge7(10,20,30);
    System.out.println("Replace Temp with Query");
    System.out.println("getTotalBill(): " + rc7.getTotalBill());   
    

    System.out.println("--Ch4-Triangle");
    
    Ch4Triangle1 t1 = new Ch4Triangle1(new Point(0,0), new Point(1,1), new Point (2,2));     
  
    Ch4ColoredTriangle1 rct1 = new Ch4ColoredTriangle1(Color.red, new Point(0,0),new Point (1,1), new Point (2,2));
    System.out.println("##Version 1");
    System.out.println(t1.equals(rct1));
    System.out.println(rct1.equals(t1)+"\n");

    /*Version2*/
    Ch4ColoredTriangle2 rct2 = new Ch4ColoredTriangle2(Color.red,new Point(0,0),new Point (1,1), new Point(2,2));
    Ch4ColoredTriangle2 bct2 = new Ch4ColoredTriangle2(Color.blue, new Point (0,0), new Point (1,1 ), new Point(2,2));
    System.out.println("##Version 2");
    System.out.println(t1.equals(rct2));
    System.out.println(rct2.equals(t1));
    System.out.println(rct2.equals(bct2) + "\n");
    
    /*Version3*/
    Ch4Triangle2 t2 = new Ch4Triangle2(new Point(0,0),new Point(1,1), new Point(2,2));
    Ch4ColoredTriangle3 rct3= new Ch4ColoredTriangle3(Color.red, new Point(0,0), new Point (1,1), new Point (2,2));
    Ch4ColoredTriangle3 bct3= new Ch4ColoredTriangle3(Color.blue, new Point(0,0),new Point (1,1), new Point(2,2));
    System.out.println("##Version 3");
    System.out.println(t2.equals(rct3));
    System.out.println(rct3.equals(t2));
    System.out.println(rct3.equals(bct3)+"\n");
    /*
    System.out.println("-reflexive : t3.equals(t3) -> "+t3.equals(t3) + "\n");
    System.out.println("-symmetric : t3.equals(rct4)  ->"+t3.equals(rct4));
    System.out.println("             rct4.equals(Triangle)  -> "+rct4.equals(t3)+"\n");
    System.out.println("-transitive : t3.equals(rct4)  -> "+t3.equals(rct4));
    System.out.println("              rct4.equals(t3)  -->"+rct4.equals(t3));
    System.out.println("              rct4.equals(bct4)  -->"+ rct4.equals(bct4) +"\n");
    System.out.println("-consistent : t3.equals(t3) -->"+t3.equals(t3)+"\n");
    System.out.println("-non_null : t3.equals(null) -->"+ t3.equals(null));
*/
}
    private static void ch3main() {
    System.out.println("--Ch3-Rectangle, MutableSquare");
    System.out.println("Rectangle width:30, height:60");
    Ch3Rectangle rec = new Ch3Rectangle(10,10,30,60);
    System.out.println("Width : "+rec.getWidth());
    System.out.println("Height : "+rec.getHeight());

    System.out.println("MutableSquare width:30, height:30");
    Ch3MutableSquare mSq = new Ch3MutableSquare(10,10,30);
    System.out.println("Width : "+mSq.getWidth());
    System.out.println("height : "+mSq.getHeight());
 
    System.out.println("--Ch3-Person, Student");
    Ch2Person ps = new Ch2Person("Lim Person", new Date());
    Ch3Student std = new Ch3Student(ps, (float)4.5);
    System.out.println("##Person (superclass)");
    System.out.println("name: "+ps.getName()+","+"birthdate: "+ps.getBirthdate());
    System.out.println("##Student (subclass)");
    System.out.println("name: "+std.getName()+","+"birthdate: "+std.getBirthdate()+"Grade: "+std.getGrade());
  
    System.out.println("--Ch3- Sorter");
    String[] B= {"John", "Adams", "Skrien", "Smith", "Jones"};
    Ch3Comparator stringComp = new Ch3StringComparator();
    Ch3Sorter.sort(B, stringComp);
    System.out.println("## StringComparator");
    for(int i=0; i<B.length; i++)
        System.out.println(B[i]);

    Integer[] C = {new Integer(3), new Integer(1), new Integer(4), new Integer(2)};
    Ch3Comparator integerComp = new Ch3IntegerComparator();
    Ch3Sorter.sort(C,integerComp);  
    System.out.println("## IntegerComparator");
    for(int i=0; i<C.length; i++)
        System.out.println(C[i]);
 }
    private static void ch2main() {
        System.out.println("--ch2-object");
        System.out.println("--Ch2-EnhancedRectagle and Oval and FilledOval");
            EnhancedRectangle rectangle = new EnhancedRectangle(3,4,60,60);
            rectangle.setLocation(10,10);
            rectangle.setCenter(20,20);
            Graphic g = new Graphic();
            g.setSize(100,100);        
            g.setVisible(true);
        System.out.println("--ch2-array vs linked");
            Listvs listvs = new Listvs(new ArrayList<String>());
            System.out.println("Add to List");
            listvs.addLi("a");
            listvs.addLi("b");
            listvs.addLi("c");
            listvs.iter();
            System.out.println("Size of List");
            listvs.sizeLi();
            System.out.println("Clear a List");
            listvs.clearLi();
        System.out.println("--Ch2-Automobile polymorphism");
        int totalCapacity=0;
        Automobile[] car = new Automobile[3];
        car[0] = new Sedan(1);
        car[1] = new Minivan(2);
        car[2] = new SportsCar(3);
        
        // version1 : instanceof
        for(int i=0; i<car.length; i++)
        {
            if(car[i] instanceof Sedan)
                totalCapacity += ((Sedan)car[i]).getCapacity();
            else if(car[i] instanceof Minivan)
                totalCapacity += ((Minivan)car[i]).getCapacity();
            else if(car[i] instanceof SportsCar)
                 totalCapacity += ((SportsCar)car[i]).getCapacity();
            else
                totalCapacity += car[i].getCapacity();
         }
        System.out.println("totalCapacity= "+totalCapacity+"(instanceof)");

       totalCapacity=0;
       // version 2 : polymorphism
       for(int i=0; i<car.length; i++)
        {
            totalCapacity += car[i].getCapacity();
        }
        System.out.println("totalCapacity = "+totalCapacity +"(polymorphism)");

    }
}
