package com.ex.ood;

import java.awt.Color;
import java.awt.Point;
import com.ex.ood.Ch4Triangle2;

public class Ch4ColoredTriangle3 extends Ch4Triangle2{
    private Color color;

    public Ch4ColoredTriangle3 ( Color c, Point p1, Point p2, Point p3){
        super(p1, p2,p3);
        if(c==null) c= Color.red;
        color =c;
    }
    public boolean equals (Object obj){
        if(obj ==null) return false;
        if(obj.getClass()!= this.getClass()) return false;
        if(!super.equals(obj)) return false;
        Ch4ColoredTriangle3 otherColoredTriangle = (Ch4ColoredTriangle3)obj;
        return this.color.equals(otherColoredTriangle.color);
    }
}
