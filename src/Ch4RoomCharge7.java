package com.ex.ood;

public class Ch4RoomCharge7
{
    //Korea Hotel
    private int room;
    private int meal;
    private int movie;
    private int sum;

    public Ch4RoomCharge7(int room, int meal, int movie){
        this.room = room;
        this.meal = meal;
        this.movie = movie;
    }
    public int getTotalBill()
    {
        return getRoomCharge() + getMealCharge() + getMovieCharge();
   }
    public int getRoomCharge()
    {
        return this.room;
    }
    public int getMealCharge()
    {
        return this.meal;
    }
    public int getMovieCharge()
    {
        return this.movie;
    }
}
