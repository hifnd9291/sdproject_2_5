package com.ex.ood;

public class Ch4RoomCharge5
{
    //Korea Hotel
    private int room;
    private int meal;
    private int movie;
    private int sum;

    public Ch4RoomCharge5(int room, int meal, int movie){
        this.room = room;
        this.meal = meal;
        this.movie = movie;
    }
    public int getTotalBill()
    {
        sum = room + meal + movie ;
        return sum;
   } 
}                          
