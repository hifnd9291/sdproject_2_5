package com.ex.ood;

public class Ch4RoomCharge1
{
    //Korea Hotel
    private int room;
    private int meal;
    private int movie;
    private int sum;

    public Ch4RoomCharge1(int room, int meal, int movie){
        this.room = room;
        this.meal = meal;
        this.movie = movie;
    }
    public int getRoomCharge() 
    {
        sum = room + meal + movie ;
        return sum;
    }     
}
