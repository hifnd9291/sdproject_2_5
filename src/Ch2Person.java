package com.ex.ood;

import java.util.Date;

public class Ch2Person
{
    private String name;
    private Date birthdate;
    
    public Ch2Person(String name, Date birthdate)
    {
        this.name=name;
        this.birthdate=birthdate;
    }
    public String getName(){
        return name;
    }
    public Date getBirthdate(){
        return birthdate;
    }
}
